from collections import OrderedDict
from commitizen.cz.base import BaseCommitizen
from commitizen.cz.utils import multiple_line_breaker, required_validator
from commitizen.defaults import MAJOR, MINOR, PATCH
from commitizen import cmd
from typing import List, Optional, Dict, Any
import os


def parse_scope(text):
    if not text:
        return ""

    scope = text.strip().split()
    if len(scope) == 1:
        return scope[0]

    return "-".join(scope)


def parse_subject(text):
    if isinstance(text, str):
        text = text.strip(".").strip()

    return required_validator(text, msg="Subject is required.")

def get_current_branch() -> Optional[str]:
    c = cmd.run("git branch --show-current")
    if c.err:
        return None
    return c.out.strip

class CommitizenOcaCz(BaseCommitizen):
    bump_pattern = r"^(BREAKING[\-\ ]CHANGE|\[REL\]|^\[IMP\]|^\[FIX\]|^\[REF\]\|^\[REV\]|^\[MERGE\]|^\[I18N\])(\(.+\))?(!)?"
    bump_map = OrderedDict(
        (
            (r"^.+!$", MAJOR),
            (r"^BREAKING[\-\ ]CHANGE", MAJOR),
            (r"^\[REL\]", MAJOR),
            (r"^\[REL\]", MINOR),
            (r"^\[IMP\]", PATCH),
            (r"^\[FIX\]", PATCH),
            (r"^\[REF\]", PATCH),
            (r"^\[REV\]", PATCH),
            (r"^\[MERGE\]", PATCH),
            (r"^\[I18N\]", PATCH),
        )
    )
    
    def questions(self) -> list:
        questions: List[Dict[str, Any]] = [
            {
                "type": "list",
                "name": "prefix",
                "message": "Select the type of change you are committing",
                "choices": [
                    {
                        "value": "IMP",
                        "name": (
                            "[IMP]: for improvements"
                            " most of the changes done in development version "
                            "are incremental improvements"
                        )
                    },                    
                    {
                        "value": "FIX",
                        "name": "[FIX]: for bug fixes",
                    },
                    {
                        "value": "REF",
                        "name": (
                            "[REF]: for refactoring "
                            "(when a feature is heavily rewritten)"
                        ),
                    },

                    {
                        "value": "ADD",
                        "name": "[ADD]: for adding new modules",
                    },
                    {
                        "value": "REM",
                        "name": (
                            "[REM]: for removing of resources "
                            "(removing dead code, removing views, removing modules, …)"
                        ),
                    },
                    {
                        "value": "REV",
                        "name": (
                            "[REV]: if a commit causes issues or is not wanted "
                            "reverting it is done using this tag"
                        ),
                    },
                   {
                        "value": "MOV",
                        "name": (
                            "[MOV]: for moving files and code "
                            "(use git move and do not change content of moved file)"
                        ),
                    },
                   {
                        "value": "REL",
                        "name": (
                            "[REL]: for release commits"
                            " new major or minor stable versions"
                        ),
                    },
                   {
                        "value": "MERGE",
                        "name": "[MERGE]: for merge commits"
                    },
                    {
                        "value": "I18N",
                        "name": "[I18N]: for changes in translation files"
                    },                    
                    {
                        "value": "CLA",
                        "name": "[CLA]: for signing the Odoo Individual Contributor License;"
                    },
                ],
            },
            {
                "type": "input",
                "name": "scope",
                "message": (
                    "Scope. Could be anything specifying place of the "
                    "commit change (module,model,view,...):\n"
                ),
                "filter": parse_scope,
            },
            {
                "type": "input",
                "name": "subject",
                "filter": parse_subject,
                "message": (
                    "Subject. Concise description of the changes. "
                    "Imperative, lower case and no final dot:\n"
                ),
            },
            {
                "type": "input",
                "name": "closes",
                "message": "Tasks ID(s) closed by this commit separated by spaces (optional):\n",
                "filter": lambda x: x.strip() if x else "",
            },                        
            {
                "type": "input",
                "name": "tasks",
                "message": "Tasks ID(s) related separated by spaces (optional):\n",
                "filter": lambda x: x.strip() if x else "",
            },            
            {
                "type": "confirm",
                "message": "Is this a BREAKING CHANGE?",
                "name": "is_breaking_change",
                "default": False,
            },
            {
                "type": "input",
                "name": "body",
                "message": (
                    "Body. Motivation for the change and contrast this "
                    "with previous behavior:\n"
                ),
                "filter": multiple_line_breaker,
            },
        ]
        return questions


    def message(self, answers: dict) -> str:
        prefix = answers["prefix"]
        scope = answers["scope"]
        subject = answers["subject"]
        body = answers["body"]
        is_breaking_change = answers["is_breaking_change"]
        tasks = answers["tasks"]
        closes = answers["closes"]
        if scope:
            scope = f"{scope}"
        if is_breaking_change:
            body = f"BREAKING CHANGE: {body}"
        if body:
            body = f"\n\n{body}"
        if closes: 
            close_text = ' '.join([f' Closes #{close_id}' for close_id in closes.split()])
            body+=close_text

        if tasks:
            extra = ''
            tasks_text = ' '.join([f'#{task_id}' for task_id in tasks.split()])
            extra += f" >>> Related Tasks: {tasks_text}"
            body+=extra
        message = f"[{prefix}] {scope}: {subject}{body}"

        return message


    def example(self) -> str:
        return (
            "[FIX] website: correct minor typos in code\n"
            "\n"
            "see the issue for details on the typos fixed\n"
            "\n"
            "closes issue #12"
        )

    def schema(self) -> str:
        return (
            "[<TAG>](<scope>): <subject>\n"
            "<BLANK LINE>\n"
            "<body>\n"
            "<BLANK LINE>\n"
            "(BREAKING CHANGE: )<footer>"
        )

    def schema_pattern(self) -> str:
        PATTERN = (
            r"(\[FIX\]|\[IMP\]|\[REF\]|\[ADD\]|\[REM\]|\[REV\]|\[MOV\]|\[REL\]|\[IMP\]|\[MERGE\]|\[CLA\]|\[I18N\]|\[MIG\])"
            r"(\(\S+\))?!?:(\s.*)"
        )
        return PATTERN

    def info(self) -> str:
        dir_path = os.path.dirname(os.path.realpath(__file__))
        filepath = os.path.join(dir_path, "oca_info.txt")
        with open(filepath, "r") as f:
            content = f.read()
        return content

discover_this = CommitizenOcaCz
